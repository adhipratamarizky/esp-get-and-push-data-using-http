#include <WiFi.h>
#include <HTTPClient.h>
#include <Arduino_JSON.h>

const char* ssid = "Primatama 64-65";
const char* password = "Soeparto64-65";

//Your Domain name with URL path or IP address with path
const char* getDataApi = "http://192.168.1.6:8000/api/data/control?nodeId=1&lantai=1";
const char* pushDataApi = "http://192.168.1.6:8000/api/data/input";

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long lastTimeGetData = 0;
unsigned long lastTimeSendData = 0;
// Timer set to 10 minutes (600000)
//unsigned long timerDelay = 600000;
// Set timer to 5 seconds (5000)
unsigned long getDataTimerDelay = 5000;
unsigned long sendDataTimerDelay = 60000;

String sensorReadings;
String sensorReadingsArr[22];

int nodeId = 1;
int lantai = 1;
int kipas1 = 1;
int kipas2 = 1;
int kipas3 = 1;
int kipas4 = 1;
int kipas5 = 1;
int kipas6 = 1;
int kipas7 = 1;
int kipas8 = 1;
int pump = 1;
int hari =1;
int colpad = 1;
int heater1 = 0;
int heater2 = 0;
int sync = 0;
float amonia = 83.50;
float temperature = 27.00;
float humidity = 63.00;
float setpoint_suhu = 29.00;
int mode_kontrol = 0;

void setup() {
  Serial.begin(9600);

  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  while(WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());
 
  Serial.println("Timer set to 5 seconds (timerDelay variable), it will take 5 seconds before publishing the first reading.");
}
void pushData(){
    WiFiClient client;
    HTTPClient http;
    
    // Your Domain name with URL path or IP address with path
    http.begin(client, pushDataApi);
    
    // Specify content-type header
    http.addHeader("Content-Type", "application/x-www-form-urlencoded");

    String httpRequestData ="&nodeId=" + String(nodeId) + "&lantai=" + String(lantai) + "&kipas1=" + String(kipas1) + "&kipas2=" + String(kipas2) + "&kipas3=" + String(kipas3) + "&kipas4=" + String(kipas4) + "&kipas5=" + String(kipas5) + "&kipas6="+ String(kipas6) 
                           + "&kipas7="+ String(kipas7) + "&kipas8="+ String(kipas8) + "&pump=" + String(pump) + "&heater1="+ String(heater1) + "&heater2="+ String(heater2) + "&colpad="+ String(colpad) + "&sync=" + String(sync) 
                           + "&temperature=" + String(temperature) + "&humidity=" + String(humidity) + "&amonia=" + String(amonia) + "&hari=" + String(hari) + "&setpoint_suhu=" + String(setpoint_suhu)
                           + "&mode=" + String(mode_kontrol);
    Serial.print("httpRequestData: ");
    Serial.println(httpRequestData);
    
    // You can comment the httpRequestData variable above
    // then, use the httpRequestData variable below (for testing purposes without the BME280 sensor)
    //String httpRequestData = "api_key=tPmAT5Ab3j7F9&sensor=BME280&location=Office&value1=24.75&value2=49.54&value3=1005.14";

    // Send HTTP POST request
    int httpResponseCode = http.POST(httpRequestData);
     
    // If you need an HTTP request with a content type: text/plain
    //http.addHeader("Content-Type", "text/plain");
    //int httpResponseCode = http.POST("Hello, World!");
    
    // If you need an HTTP request with a content type: application/json, use the following:
    //http.addHeader("Content-Type", "application/json");
    //int httpResponseCode = http.POST("{\"value1\":\"19\",\"value2\":\"67\",\"value3\":\"78\"}");
        
    if (httpResponseCode>0) {
      Serial.print("HTTP Response code: ");
      Serial.println(httpResponseCode);
    }
    else {
      Serial.print("Error code: ");
      Serial.println(httpResponseCode);
    }
    // Free resources
    http.end();
}

void getData(){
      sensorReadings = httpGETRequest(getDataApi);
      Serial.println(sensorReadings);
      JSONVar myObject = JSON.parse(sensorReadings);
  
      // JSON.typeof(jsonVar) can be used to get the type of the var
      if (JSON.typeof(myObject) == "undefined") {
        Serial.println("Parsing input failed!");
        return;
      }
    
      Serial.print("JSON object = ");
      Serial.println(myObject);
    
      // myObject.keys() can be used to get an array of all the keys in the object
      JSONVar keys = myObject.keys();
//      Serial.println(keys);
//      Serial.println(keys.length());
      //error di looping ini, index out of bounds
      for (int i = 0; i < keys.length(); i++) {
        JSONVar value = myObject[keys[i]];
        Serial.print(keys[i]);
        Serial.print(" = ");
        Serial.println(value);
//        sensorReadingsArr[i] = double(value);
        sensorReadingsArr[i] = value;
        delay(500);
      }
      delay(500);
//      for(int i=0; i<22; i++){
//        Serial.print(i);
//        Serial.print("=");
//        Serial.println(sensorReadingsArr[i]);
//        delay(500);
//      }
//      Serial.print("1 = ");
//      Serial.println(sensorReadingsArr[0]);
//      Serial.print("2 = ");
//      Serial.println(sensorReadingsArr[1]);
//      Serial.print("3 = ");
//      Serial.println(sensorReadingsArr[2]);
//      }
 }

String httpGETRequest(const char* getDataApi) {
  WiFiClient client;
  HTTPClient http;
    
  // Your Domain name with URL path or IP address with path
  http.begin(client, getDataApi);
  
  // Send HTTP POST request
  int httpResponseCode = http.GET();
  
  String payload = "{}"; 
  
  if (httpResponseCode>0) {
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
    payload = http.getString();
    payload.replace("[","");
    payload.replace("]","");
  }
  else {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }
  // Free resources
  http.end();
  delay(500);
  
  return payload;
}

void loop() {
    //Check WiFi connection status
    if(WiFi.status()== WL_CONNECTED){
      if ((millis() - lastTimeGetData) > getDataTimerDelay) {            
        getData();
        lastTimeGetData = millis();
        delay(500);
      }
      if ((millis() - lastTimeSendData) > sendDataTimerDelay) {            
        pushData();
        lastTimeSendData = millis();
        delay(500);
      }
    }
    else {
      Serial.println("WiFi Disconnected");
    }  
}
